const LocalStrategy = require('passport-local'),
  passport = require('passport'),
  saltRounds = 10,
  userAuth = require('../model/userAuth');
users = require('../models').User;


const promises = require('./promises');


exports.passConf = function() {

  passport.serializeUser((user_id, done) => {
    done(null, user_id);
  });

  passport.deserializeUser((user_id, done) => {
    done(null, user_id);
  });


  passport.use('local-signup', new LocalStrategy({
    passReqToCallback: true
  }, (req, username, password,
    done) => {
    const email = req.body.email;
    users.checkForExisting(username, email)
      .then((result) => {
        if (result[0])
          return done(null, false);
        return promises.bcryptPromiseHash(password, saltRounds);
      })
      .then((hash) => {
        console.log('HERE WE GO');
        console.log(hash);
        return users.InsertRegisterUser(username, email, hash);
      })
      .then(id => {
        return done(null, {
          user_id: id
        });
      }).catch(err => done(err));

  }));

  passport.use('local-login', new LocalStrategy({
    passReqToCallback: true
  }, (req, username, password, done) => {
    let res;
    users.userLogIn(username)
      .then(result => {
        if (result.length === 0)
          return done(null, false);
        const hash = result[0].password.toString();
        res = result;
        return promises.bcryptPromiseCompare(password, hash);
      }).then(response => {
        console.log('LOG INNN');
        console.log(response);
        if (response === true) {
          return done(null, {
            user_id: res[0].id
          });
        } else return done(null, false);
      }).catch(err => done(err));
  }));
}
