 //const promises = require('../config/promises');
 const Users = require('../models').UserInfo;



 exports.printUser = (tip) => {

   return Users.getAllUsers(tip)
     .then(result => result)
     .catch(err => new Error(err));
 }


 exports.insertNewUser = (newObj) => {

   return Users.newUserQuery(newObj)
     .then(res => true)
     .catch(err => new Error(err));

 };


 exports.deleteUser = (id) => {

   return Users.deleteUserQuery(id)
     .then(res => true)
     .catch(err => new Error(err));
 }

 exports.updateUser = (newObj) => {

   return Users.editUserQuery(newObj.obj, newObj.id)
     .then(res => newObj.obj)
     .catch(err => new Error(err));

 }
