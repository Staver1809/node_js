const bcrypt = require('bcrypt');


exports.promiseQuery = (userFunc, ...obj) => {
  return new Promise((resolve, reject) => {
    userFunc(obj, (err, result) => {
      if (err)
        reject(err);

      resolve(result);
    });
  });
}

exports.bcryptPromiseHash = (password, saltRounds) => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, saltRounds, (err, hash) => {
      if (err)
        reject(err);
      resolve(hash);
    });
  });
}

exports.bcryptPromiseCompare = (password, hash) => {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, hash, (err, response) => {
      if (err)
        reject(err);
      resolve(response);
    });
  });
}
