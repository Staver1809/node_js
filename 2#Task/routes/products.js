const express = require('express');
const router = express.Router();
const productController = require('../controlers/productController');


router.route('/')
  .get((req, res) => {
    productController.printAll()
      .then(result => {
        res.render('products', {
          products: result
        });
      })
      .catch(err => {
        res.status(400).send(err);
      });
  })

router.route('/form')
  .post((req, res) => {

    productController.insertNewProduct(req.body)
      .then(response => {
        res.redirect('/products');
      })
      .catch(err => {
        res.status(400).send(err);
      });
  })

router.route('/id-:nr')
  .delete((req, res) => {
    productController.deleteProduct(req.params.nr)
      .then(() => {
        res.sendStatus(200);
      })
      .catch(err => {
        res.status(400).send(err);
      });
  })
  .put((req, res) => {
    let data = {
      obj: req.body,
      id: req.params.nr
    };
    productController.updateProduct(data)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        res.status(400).send(err);

      });
  });

module.exports = router;
