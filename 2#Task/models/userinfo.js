'use strict';
module.exports = (sequelize, DataTypes) => {
  var UserInfo = sequelize.define('UserInfo', {
    Nume: {
      type: DataTypes.STRING,
      defaultValue: ' '
    },
    Prenume: {
      type: DataTypes.STRING,
      defaultValue: ' '
    },
    Adresa: {
      type: DataTypes.STRING,
      defaultValue: ' '
    },
    Telefon: {
      type: DataTypes.STRING,
      defaultValue: ' '
    },
    Tip: {
      type: DataTypes.STRING,
      defaultValue: ' '
    },
    UserId: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  });



  UserInfo.associate = function(models) {
    UserInfo.belongsToMany(models.Products, {
      through: 'UserInfo_Products'
    });
  }

  UserInfo.getAllUsers = function(type) {
    return UserInfo.findAll({
      attributes: ['id', 'Nume', 'Prenume', 'Adresa', 'Telefon', 'Tip'],
      where: {
        Tip: type
      }
    });
  }


  UserInfo.newUserQuery = function(newObj) {
    return UserInfo.create(newObj);
  }

  UserInfo.editUserQuery = function(newObj, objId) {
    return UserInfo.update(newObj, {
      where: {
        id: parseInt(objId)
      }
    });
  }
  UserInfo.deleteUserQuery = function(objId) {
    return UserInfo.destroy({
      where: {
        id: parseInt(objId)
      }
    });
  }

  UserInfo.getUserInfo = (objId) => {
    return UserInfo.findAll({
      attributes: ['id', 'Nume', 'Prenume', 'Adresa', 'Telefon', 'Tip'],
      where: {
        UserId: objId
      }
    });
  }
  UserInfo.insertUserInfo = (id, obj) => {
    obj.UserId = id;
    return UserInfo.create(obj);
  }

  return UserInfo;
}
