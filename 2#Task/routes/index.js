const express = require('express'),
  path = require('path'),
  router = express.Router(),
  products = require('./products'),
  clients = require('./clients'),
  sellers = require('./sellers'),
  expressValidator = require('express-validator'),
  bcrypt = require('bcrypt'),
  passport = require('passport'),
  authentification = require('./authentification');

const saltRounds = 10;
let session;



router.use('/authentification', authentification);
router.use('/products', products);
router.use('/clients', clients);
router.use('/sellers', sellers);


router.get('/', (req, res) => {
  res.render('index');
});

module.exports = router;
