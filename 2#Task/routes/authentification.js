const express = require('express');
const router = express.Router(),
  passport = require('passport'),
  authController = require('../controlers/authentification'),
  promises = require('../config/promises');


let Errors = [];

router.route('/login')
  .get((req, res) => {
    console.log('This are errors');
    console.log(Errors);
    res.render('login', {
      errors: Errors
    });
  })
  .post(loginErrorHandling(), (req, res, next) => {
    passport.authenticate('local-login', (err, user, info) => {
      if (err) {
        return next(err);
      }
      if (!user) {
        res.render('login', {
          errors: [{
            msg: 'Incorect username or password'
          }]
        });
      }
      req.logIn(user, (err) => {
        if (err) {
          return next(err);
        };
        authController.getUserInfo(req.user.user_id)
          .then(obj => {
            session = req.session;
            session.userObj = obj;
            res.redirect('/authentification/dashboard');
          });
      });
    })(req, res, next);
  });

router.get('/logout', (req, res) => {
  req.logout();
  req.session.destroy();
  res.redirect('/');
});

router.route('/register/registerForm')
  .get((req, res) => {
    res.render('register', {
      errors: Errors
    });
    Errors = [];
  })
  .post(signupErrorHandling(), (req, res, next) => {
    passport.authenticate('local-signup', (err, user, info) => {
      console.log('CHECK');
      if (err) return next(err);

      console.log('2 CHECK');
      if (!user) {
        res.render('register', {
          errors: [{
            msg: 'Username or email already exists'
          }]
        });
      }

      console.log('3 CHECK');
      req.logIn(user, (err) => {
        console.log('4 CHECK');
        if (err) {
          return next(err);
        }
        res.send('ok');
      })
    })(req, res, next);
  });

router.route('/register/userInfoForm')
  .get((req, res) => {
    res.render('register', {
      errors: null
    });
  })
  .post((req, res, next) => {
    authController.insertUserInfo(req.user.user_id, req.body)
      .then(() => {
        return authController.getUserInfo(req.user.user_id);
      })
      .then(obj => {
        console.log('HERE I AM?');
        session = req.session;
        session.userObj = obj;;
        res.send('ok');
      })
      .catch(err => res.status(400).send(err));
  });

router.route('/dashboard')
  .get(authentificationMiddleware(), (req, res) => {
    session = req.session;
    res.render('dashboard', {
      userDash: session.userObj
    });
  });


function signupErrorHandling() {

  return function(req, res, next) {
    req.checkBody('username', 'Username field cannot be empty').notEmpty();
    req.checkBody('username', 'Username myst be betweeb 4-15 characters long')
      .len(4, 15);
    req.checkBody('email',
      'The email you entered is invalid, please try again').isEmail();
    req.checkBody('email', 'Email address must be 4-199').len(4, 100);
    req.checkBody('password', 'Password must be 8-100').len(8, 100);
    req.checkBody('passwordMatch', 'Betweenn 8-100').len(8, 100);
    req.checkBody('passwordMatch', 'Passwords do not match').equals(req.body.password);
    const errors = req.validationErrors();
    if (errors) {
      console.log(`errors: ${JSON.stringify(errors)}`);
      Errors = errors;
      res.send(null);
      return;
    }

    next();
  }
}

function loginErrorHandling() {
  return (req, res, next) => {
    req.checkBody('username', 'Username field cannot be empty').notEmpty();
    req.checkBody('username', 'Username myst be betweeb 4-15 characters long')
      .len(4, 15);
    req.checkBody('password', 'Password must be 8-100').len(8, 100);

    const errors = req.validationErrors();
    if (errors) {
      console.log(`errors: ${JSON.stringify(errors)}`);
      //  Errors = errors;
      res.render('login', {
        errors: errors
      });
      //res.send(null);
      return;
    }
    next();
  }
}

function authentificationMiddleware() {
  return (req, res, next) => {
    if (req.isAuthenticated()) return next();
    res.redirect('/login');
  }
}


module.exports = router;
