const express = require('express');
const router = express.Router();


const userController = require('../controlers/userController');


router.route('/')
  .get((req, res) => {
    userController.printUser('client')
      .then(result => {
        res.render('users', {
          users: result
        });
      })
      .catch(err => {
        res.status(400).send(err);
      });
  });

router.route('/form')
  .post((req, res) => {
    userController.insertNewUser(req.body)
      .then(result => {
        res.redirect('/clients');
      })
      .catch(err => {
        res.status(400).send(err);
      });

  });

router.route('/id-:nr')
  .put((req, res) => {
    console.log('BLAA');
    console.log(req.body);
    let data = {
      obj: req.body,
      id: req.params.nr
    }
    userController.updateUser(data)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        res.status(400).send(err);
      });
  })
  .delete((req, res) => {
    userController.deleteUser(req.params.nr)
      .then(result => {
        res.sendStatus(200);
      })
      .catch(err => {
        res.status(400).send(err);
      });
  });

module.exports = router;
