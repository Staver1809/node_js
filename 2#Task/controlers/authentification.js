const User = require('../model/userAuth'),
  usersInfo = require('../models').UserInfo;

exports.getUserInfo = (userId) => {

  return usersInfo.getUserInfo(userId)
    .then((result) => {
      let obj = {
        id: result[0].dataValues.Nume,
        username: result[0].dataValues.Prenume,
        email: result[0].dataValues.Tip
      }
      return obj;

    }).catch(err => {
      return new Error(err);
    })

}

exports.insertUserInfo = (id, newObj) => {
  return usersInfo.insertUserInfo(id, newObj)
    .then(res => true);
}
