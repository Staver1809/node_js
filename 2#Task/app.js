const express = require('express'),
  path = require('path');
routes = require('./routes'),
  bodyParser = require('body-parser'),
  dotenv = require('dotenv'),
  expressValidator = require('express-validator'),
  session = require('express-session'),
  passport = require('passport'),
  LocalStrategy = require('passport-local'),
  MySQLStore = require('express-mysql-session')(session),
  bcrypt = require('bcrypt'),
  passportConfig = require('./config/passport');


app = express();

dotenv.config();
//global.db = require('./db');

const urlencodedParse = bodyParser.urlencoded({
  extended: false
});


app.set('view engine', 'ejs');

app.use(urlencodedParse);
app.use(expressValidator());
app.use('/static', express.static(path.join(__dirname, 'public')));
app.use((req, res, next) => {
  passportConfig.passConf();
  next();
});

const options = {
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  socketPath: `/opt/lampp/var/mysql/mysql.sock`
};

const sessionStore = new MySQLStore(options);

app.use(session({
  secret: 'sdfasdfasfvcsadf',
  resave: false,
  store: sessionStore,
  saveUninitialized: false,
}));

app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  res.locals.isAuthenticated = req.isAuthenticated();
  next();
});

app.use(function(req, res, next) {
  res.set('Cache-Control',
    'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0'
  );
  next();
});

app.use('/', routes);

/*app.use((req, res) => {
  res.send('WTF');
});*/

app.listen(process.env.PORT_NUMBER, () => {

  console.log(`App started at port 3000sssssss`);

});
