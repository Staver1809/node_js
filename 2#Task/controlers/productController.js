const Products = require('../models').Products;


exports.printAll = () => {

  return Products.getAllProducts()
    .then(result => result)
    .catch(err => new Error(err));
};

exports.deleteProduct = (objId) => {

  return Products.deleteProductQuery(objId)
    .then(res => true)
    .catch(err => new Error(err));
}

exports.insertNewProduct = (newObj) => {
  console.log(`NEW OBJECT`);
  console.log(newObj);

  return Products.newProductQuery(newObj)
    .then(res => true)
    .catch(err => new Error(err));
}

exports.updateProduct = (newObj) => {

  return Products.editProductQuery(newObj.obj, newObj.id)
    .then(res => newObj.obj)
    .catch(err => new Error(err));
}
