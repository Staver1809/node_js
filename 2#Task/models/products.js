'use strict';

module.exports = (sequelize, DataTypes) => {
  var Products = sequelize.define('Products', {
    Nume: DataTypes.STRING,
    Categorie: DataTypes.STRING,
    Pret: DataTypes.STRING
  });

  Products.associate = function(models) {
    Products.belongsToMany(models.UserInfo, {
      through: 'UserInfo_Products'
    });
  }

  Products.getAllProducts = function() {
    return Products.findAll();
  }

  Products.newProductQuery = function(newObj) {
    return Products.create(newObj);
  }

  Products.editProductQuery = function(newObj, objId) {
    return Products.update(newObj, {
      where: {
        id: parseInt(objId)
      }
    });
  }
  Products.deleteProductQuery = function(objId) {
    return Products.destroy({
      where: {
        id: parseInt(objId)
      }
    });
  }


  return Products;
};
