$(function() {
  let editClickState = true;

  $('.table').on('click', '#deleteButton', function(event) {
    event.preventDefault();
    event.stopPropagation();
    editClickState = true;
    let configurations = getConfigurations($(this));
    let finalRequest =
      `${configurations.requestURL}/id-${configurations.currentRowId.id}`;
    console.log(`ID =`);
    console.log(configurations.requestURL);
    $.ajax({
      url: finalRequest,
      type: 'DELETE',
      data: configurations.currentRowId
    }).done((data) => {
      removeRow($(this), '#FF8080');
      console.log('In alright' + data);

    }).fail(function(data) {
      console.log('Im not alright');
    });
  });


  $('.table').on('click', '#editButton', function(event) {
    event.preventDefault();
    event.stopPropagation();

    if (editClickState) {

      let configurations = getConfigurations($(this));
      let arrItem = getRowCollumsValues($(this));
      let finalRequest =
        `${configurations.requestURL}/id-${configurations.currentRowId.id}`;
      let editRowSubmitID;
      const tableCollumNames = getTableCollumsText($(this));

      createNewEditRowTemplate($(this), tableCollumNames);
      setDefaultCollumValues($(this), arrItem);

      editRowSubmitID =
        `#${$(this).parents('tr').next().children('form').attr('id')}`;

      editClickState = false;
      $(editRowSubmitID).submit(function(event) {
        event.preventDefault();
        event.stopPropagation();
        let info = $(editRowSubmitID).serialize();
        console.log(finalRequest);
        console.log('HEEEEY');
        $.ajax({
          url: finalRequest,
          type: 'PUT',
          data: info
        }).done((data) => {
          console.log($(this));
          removeRow($(this), '#FFA280');
          updateEditedRow($(this), data, arrItem);
          editClickState = true;
          console.log('Im  alright');
        }).fail((data) => {
          console.log('Im not alright');
        });
      });

    } else {
      let referenceToEditedRow = $(this).parents('tr').next().children()
        .eq(0);
      removeRow(referenceToEditedRow, '#FFA280');
      editClickState = true;
    };
  });

});


function getConfigurations(myObj) {

  let configuration = {
    tableType: myObj.parents('table').attr('id'),
    requestURL: myObj.parents('tr').attr('requestURL'),
    currentRowId: {
      id: myObj.parents('tr').attr('id')
    },
    currentRowObject: myObj.parents('tr'),
    nextRowId: myObj.parents('tr').next().attr('id')
  }
  return configuration;
};

function removeRow(myObj, color) {
  let configurations = getConfigurations(myObj);

  $(configurations.currentRowObject)
    .css("background-color", color)
    .fadeOut(500, () => {
      $(configurations.currentRowObject).remove();
    });
  if (configurations.nextRowId === 'editRow') {
    $(configurations.currentRowObject).next()
      .css("background-color", color)
      .fadeOut(500, () => {
        $(configurations.currentRowObject).next().remove();
      });
  }
}

function getTableCollumsText(myObj) {
  let obj = myObj.parents('tbody'),
    arr = [];

  for (let i = 0; i < 50; i++) {
    arr.push(obj.children().eq(0).children().eq(i).text());
    console.log(`BOOOO I AM`);
    if (!arr[i]) {
      arr.pop();
      arr.pop();
      break;
    }
  }

  return arr;
}

function createNewEditRowTemplate(myObj, tableCollumNames) {
  let newFormID = myObj.parents('tr').children().eq(0).text();
  newFormID = 'submitEditetRow' + newFormID;
  const editStringFirstPart =
    `<tr id="editRow"><form id="${newFormID}"></form>`;
  const submitButtonString =
    `<td><input type="submit" class="form-control" id="submitButton" value="submit" form="${newFormID}"></td></tr>`;

  let inputRows = [],
    tableEditRowString;


  tableCollumNames.forEach((collumName, index) => {
    if (index == 0) {
      inputRows.push(
        `<td><input type="text" name="${collumName}" class="form-control" form="${newFormID}" readOnly></td>`
      );
    } else {
      inputRows.push(
        `<td><input type="text" name="${collumName}" class="form-control" form="${newFormID}"></td>`
      );
    }
  });
  tableEditRowString = `${editStringFirstPart}`;

  inputRows.forEach((collumInputString) => {
    tableEditRowString += collumInputString;
  });
  tableEditRowString += submitButtonString;
  myObj.parents('tr').after(tableEditRowString);
  //  return tableEditRowString;
}

function getRowCollumsValues(myObj) {

  let rowValues = [];
  for (var i = 0; i < 50; i++) {
    rowValues.push(myObj.parents('tr').children().eq(i).text());
    console.log(`HERE I AM`);
    if (!rowValues[i]) {
      rowValues.pop();
      rowValues.pop();
      break;
    }
  }

  return rowValues;
}

function setDefaultCollumValues(myObj, arr) {

  arr.forEach((elem, index) => {
    myObj.parents('tr')
      .next()
      .children().eq(index + 1)
      .children().attr('value', elem);
  });
}

function updateEditedRow(myObj, data, arr) {
  let key, value;
  console.log(data);
  arr.forEach((elem, index) => {
    console.log('bla');
    myObj.parents('tr').prev().children().eq(index + 1).text(
      function() {
        key = Object.keys(data)[index + 1];
        value = data[key];
        console.log(`VALUE _ ${value}`);
        return value;
      });
  });

}
