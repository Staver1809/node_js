$(function() {
  //   $('#myTab a').tab('show');

  $('.nav-tabs li').on('click', (event) => {
    event.preventDefault();
    event.stopPropagation();
  });

  $('#registerForm').submit(function(event) {
    event.preventDefault();
    event.stopPropagation();
    let info = $(this).serialize();

    $.ajax({
        url: '/authentification/register/registerForm',
        type: 'POST',
        data: info
      })
      .done((data) => {
        //  console.log(data);
        if (data) {

          $('#myTab a').tab('show');
          $('#myTab a').css('color', '#ff6666');
          $('#myTab').siblings().children('a').css('color', 'black');
        } else {
          window.location.href =
            '/authentification/register/registerForm';
        }
      })
      .fail((data) => {
        console.log('ERROR');
      });;

  });

  $('#UserInfo').submit(function(event) {
    event.preventDefault();
    event.stopPropagation();
    let info = $(this).serialize();

    $.ajax({
      url: '/authentification/register/userInfoForm',
      type: 'POST',
      data: info
    }).done((data) => {
      window.location.href = '/authentification/dashboard';
    });
  });
});
