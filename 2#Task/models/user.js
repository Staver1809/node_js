'use strict';


module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    username: {
      type: DataTypes.STRING,
      unique: true
    },
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  });

  User.associate = function(models) {
    User.belongsTo(models.UserInfo, {
      foreignKey: {
        name: 'UserId',
        allowNull: true
      }
    });
  }
  User.checkForExisting = (username, email) => {
    return User.findAll({
      attributes: ['username', 'email'],
      where: {
        $or: [{
          username: username
        }, {
          email: email
        }]
      }
    })
  }

  User.InsertRegisterUser = (username, email, hash) => {
    let obj = {
      username: username,
      email: email,
      password: hash,
    }
    return User.create(obj)
      .then(result => {
        return result.dataValues.id;
      });
  }

  User.userLogIn = (username) => {
    console.log('USER LOGIN');
    return User.findAll({
      attributes: ['id', 'password'],
      where: {
        username: username
      }
    });
  }
  return User
};
